package com.retroroots.network.data_source

import com.retroroots.network.state.ApiError
import com.retroroots.network.state.ResponseState

interface DataSource<S>
{
    suspend fun selectById(id : Int): ResponseState<S, ApiError>

    //Not using this now because get all returns a different object
   //suspend fun selectAll(): ResponseState<S, ApiError>
}