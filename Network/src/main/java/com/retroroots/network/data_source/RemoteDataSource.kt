package com.retroroots.network.data_source

import com.retroroots.network.state.ApiError
import com.retroroots.network.state.ResponseState
import retrofit2.Response
import java.net.UnknownHostException

abstract class RemoteDataSource<S>: DataSource<S>
{
    protected fun <S2> processResponse(process : Response<S2>) : ResponseState<S2, ApiError>
    {
        process.apply {
            body()?.let {
                if(isSuccessful)
                    return ResponseState.Success(it)
            }

            return ResponseState.Failure(ApiError.ServerError(Exception()))
        }
    }

    protected fun processException(exception: Exception) : ResponseState<Nothing, ApiError> =
            when(exception)
            {
                is UnknownHostException -> ResponseState.Failure(ApiError.NoInternetConnectionError(exception))

                else -> ResponseState.Failure(ApiError.UnknownServerError(exception))
            }
}