package com.retroroots.network.state

sealed class ResponseState<out S, out E>
{
    data class Success<out S, out E>(val data : S) : ResponseState<S, E>()

    data class Failure<out S, out E>(val error : E) : ResponseState<S, E>()

    data object Loading : ResponseState<Nothing, Nothing>()
}