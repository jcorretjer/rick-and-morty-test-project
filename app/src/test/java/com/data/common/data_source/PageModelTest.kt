package com.data.common.data_source

import com.example.rickandmortycharacters.data.common.data_source.all_characters.Page
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Assert.fail
import org.junit.Test

class PageModelTest
{
    private val fakePage = Page("https://rickandmortyapi.com/api/character/?page=2", 0, null)

    @Test
    fun returnsPageNumberFromValidPageUrlTest()
    {
        assertEquals(2, Page.getPageNumberOrNull("https://rickandmortyapi.com/api/character/?page=2"))
    }

    @Test
    fun returnsNullFromInvalidPageUrlTest()
    {
        assertEquals(null, Page.getPageNumberOrNull("https://rickandmortyapi.com/api/character/?test"))
    }

    @Test
    fun returnsNullFromNullValueTest()
    {
        assertEquals(null, Page.getPageNumberOrNull(null))
    }
}