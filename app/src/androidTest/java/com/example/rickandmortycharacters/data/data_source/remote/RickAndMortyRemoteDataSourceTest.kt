package com.example.rickandmortycharacters.data.data_source.remote

import com.example.rickandmortycharacters.common.RickAndMortyDataTest
import com.example.rickandmortycharacters.data.remote.data_source.RickAndMortyRemoteDataSource
import com.retroroots.network.state.ApiError
import com.retroroots.network.state.ResponseState
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import javax.inject.Inject

@HiltAndroidTest
class RickAndMortyRemoteDataSourceTest: RickAndMortyDataTest
{
    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Inject
    lateinit var rickAndMortyRemoteDataSource: RickAndMortyRemoteDataSource

    @Before
    fun init()
    {
        hiltRule.inject()
    }

    @Test
    override fun selectByInvalidIdTest()
    {
        runBlocking {
            assertTrue((rickAndMortyRemoteDataSource.selectById(-1) as ResponseState.Failure).error is ApiError.ServerError)
        }
    }

    @Test
    override fun selectByValidIdTest()
    {
        runBlocking {
            assertTrue(rickAndMortyRemoteDataSource.selectById(1) is ResponseState.Success)
        }
    }

    @Test
    override fun selectByIdNoConnectionTest()
    {
        runBlocking {
            assertTrue((rickAndMortyRemoteDataSource.selectById(-1) as ResponseState.Failure).error is ApiError.NoInternetConnectionError)
        }
    }

    @Test
    override fun selectAllTest()
    {
        runBlocking {
            assertTrue(rickAndMortyRemoteDataSource.selectAll(0) is ResponseState.Success)
        }
    }

    @Test
    override fun selectAllPage1Test()
    {
        runBlocking {
            assertTrue(rickAndMortyRemoteDataSource.selectAll(1) is ResponseState.Success)
        }
    }

    @Test
    override fun selectAllNoConnectionTest()
    {
        runBlocking {
            assertTrue((rickAndMortyRemoteDataSource.selectAll(0) as ResponseState.Failure).error is ApiError.NoInternetConnectionError)
        }
    }
}