package com.example.rickandmortycharacters.data.data_source.remote.repo.paging

import androidx.paging.LoadState
import androidx.paging.testing.ErrorRecovery
import androidx.paging.testing.asSnapshot
import com.example.rickandmortycharacters.data.repo.paging.RickAndMortyPagedRepo
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertTrue
import org.junit.Assert.fail
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.net.UnknownHostException
import javax.inject.Inject


@HiltAndroidTest
class RickAndMortyPagingSourceTest
{
    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Inject
    lateinit var rickAndMortyPagedRepo: RickAndMortyPagedRepo

    @Before
    fun init()
    {
        hiltRule.inject()
    }

    @Test
    fun selectAllTest()
    {
        runTest {
            rickAndMortyPagedRepo.getAllCharacters().asSnapshot {
                scrollTo(50)
            }.let {
                assertTrue(it.isNotEmpty())
            }
        }
    }

    @Test
    fun selectAllNoConnectionTest()
    {
        runBlocking {
            try
            {
                rickAndMortyPagedRepo.getAllCharacters().asSnapshot({
                    assertTrue((it.refresh as LoadState.Error).error is UnknownHostException)
                    ErrorRecovery.THROW
                }) {
                    scrollTo(50)
                }.let {
                    fail()
                }
            }

            catch (e: Exception)
            {
                assertTrue(e is UnknownHostException)
            }
        }
    }
}