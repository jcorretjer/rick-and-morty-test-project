package com.example.rickandmortycharacters.common

interface RickAndMortyDataTest
{
    fun selectByInvalidIdTest()

    fun selectByValidIdTest()

    fun selectByIdNoConnectionTest()

    fun selectAllTest()

    fun selectAllPage1Test()

    fun selectAllNoConnectionTest()
}