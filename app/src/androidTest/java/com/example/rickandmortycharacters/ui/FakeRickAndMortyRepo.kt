package com.example.rickandmortycharacters.ui

import com.example.rickandmortycharacters.data.common.data_source.all_characters.AllCharacters
import com.example.rickandmortycharacters.data.common.data_source.character.Character
import com.example.rickandmortycharacters.data.repo.SharedRickAndMortyRepo
import com.retroroots.network.state.ApiError
import com.retroroots.network.state.ResponseState

/*
Took ideas from this video https://www.youtube.com/watch?v=nKCsIHWircA
*/
class FakeRickAndMortyRepo : SharedRickAndMortyRepo
{
    private lateinit var character : ResponseState<Character, ApiError>

    fun fakeCharacterServerResponse(value : ResponseState<Character, ApiError>){
        character = value
    }

    override suspend fun getCharacter(id: Int): ResponseState<Character, ApiError> = character

    private lateinit var allCharacters: ResponseState<AllCharacters, ApiError>

    fun fakeAllCharacterServerResponse(value : ResponseState<AllCharacters, ApiError>){
        allCharacters = value
    }

    override suspend fun getAllCharacters(page: Int): ResponseState<AllCharacters, ApiError> = allCharacters
}