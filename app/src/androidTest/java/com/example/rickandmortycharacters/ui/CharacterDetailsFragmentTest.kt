package com.example.rickandmortycharacters.ui

import android.content.Context
import android.net.ConnectivityManager
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.hasDescendant
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withContentDescription
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import com.example.rickandmortycharacters.R
import com.example.rickandmortycharacters.data.remote.api.RickAndMortyApi
import com.example.rickandmortycharacters.data.remote.di.RickAndMortyApiModule
import com.retroroots.util.isNetworkAvailable
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import dagger.hilt.components.SingletonComponent
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.Response
import java.net.UnknownHostException
import javax.inject.Singleton

@UninstallModules(RickAndMortyApiModule::class)
@HiltAndroidTest
class CharacterDetailsFragmentTest
{
    @Module
    @InstallIn(SingletonComponent::class)
    class FakeRickAndMortyApiModule
    {
        @Singleton
        @Provides
        fun providesRickAndMortyApi(@ApplicationContext context: Context) = object: RickAndMortyApi
        {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

            override suspend fun getCharacter(id: Int) =
                    when
                    {
                        connectivityManager.isNetworkAvailable() ->{
                            if(id < 0)
                                throw Exception()

                            Response.success(200, CharacterRosterFragmentTest.fakeCharacter)
                        }

                        else -> throw UnknownHostException()
                    }

            override suspend fun getAllCharacter(page: Int) =
                    when
                    {
                        connectivityManager.isNetworkAvailable() -> {
                            if(page < 0)
                                throw Exception()

                            Response.success(200, CharacterRosterFragmentTest.fakeAllCharacters)
                        }

                        else -> throw UnknownHostException()
                    }
        }
    }

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Before
    fun init()
    {
        hiltRule.inject()

        ActivityScenario.launch(MainActivity::class.java)

        CharacterRosterFragmentTest.navToDetailsFragment()
    }

    @Test
    fun confirmCharacterDetailIsValidTest()
    {
        onView(withId(R.id.characterDataTxtVw)).check(
            matches(
                withText(
                    "Character(created=, episode=[], gender=, id=1, image=, location=Location(name=, url=), name=test, " +
                            "origin=Origin(name=, url=), species=, status=, type=, url=)"
                )
            )
        )
    }

    @Test
    fun confirmToolbarNameIsValidTest()
    {
        onView(withId(R.id.characterToolbar)).check(matches(hasDescendant(withText("test"))))
    }

    @Test
    fun confirmToolbarBackButtonNavigatesBackHomeTest()
    {
        onView(withContentDescription(androidx.navigation.ui.R.string.nav_app_bar_navigate_up_description)).perform(click())

        onView(withId(R.id.rosterRecVw)).check(matches(isDisplayed()))
    }

    @Test
    fun confirmSystemBackButtonNavigatesBackHomeTest()
    {
        pressBack()

        onView(withId(R.id.rosterRecVw)).check(matches(isDisplayed()))
    }
}