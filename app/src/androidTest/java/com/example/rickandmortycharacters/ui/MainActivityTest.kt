package com.example.rickandmortycharacters.ui

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDescendantOfA
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import com.example.rickandmortycharacters.R
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.hamcrest.core.AllOf.allOf
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@HiltAndroidTest
class MainActivityTest
{
    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Before
    fun init()
    {
        hiltRule.inject()

        ActivityScenario.launch(MainActivity::class.java)
    }

    @Test
    fun rosterTabIsVisibleOnLaunchTest()
    {
        onView(withId(R.id.rosterRecVw)).check(matches(isDisplayed()))
    }


    //2131230835
    @Test
    fun botNavRosterTabNavigatesToRosterFragTest()
    {
        botNavPagedRosterTabNavigatesToPagedRosterFragTest()

        //2131230835 == id of the roster tab. Can't do by actual id res or text because nav comp internally adds duplicates to the view hierarchy
        onView(withId(2131230835)).perform(click())

        onView(withId(R.id.rosterRecVw)).check(matches(isDisplayed()))
    }

    @Test
    fun botNavPagedRosterTabNavigatesToPagedRosterFragTest()
    {
        //2131231076 == id of the paged roster tab. Can't do by actual id res or text because nav comp internally adds duplicates to the view hierarchy
        onView(withId(2131231076)).perform(click())

        onView(withId(R.id.rosterRecVw2)).check(matches(isDisplayed()))
    }

    @Test
    fun botNavComposeRosterTabNavigatesToComposeRosterFragTest()
    {
        TODO("Not yet implemented")
    }
}