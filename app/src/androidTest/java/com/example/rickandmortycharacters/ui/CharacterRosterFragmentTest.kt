package com.example.rickandmortycharacters.ui

import android.content.Context
import android.net.ConnectivityManager
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.hasChildCount
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Before
import org.junit.Rule
import com.example.rickandmortycharacters.R
import com.example.rickandmortycharacters.data.common.data_source.all_characters.AllCharacters
import com.example.rickandmortycharacters.data.common.data_source.all_characters.Page
import com.example.rickandmortycharacters.data.common.data_source.character.Character
import com.example.rickandmortycharacters.data.common.data_source.character.Location
import com.example.rickandmortycharacters.data.common.data_source.character.Origin
import com.example.rickandmortycharacters.data.remote.api.RickAndMortyApi
import com.example.rickandmortycharacters.data.remote.di.RickAndMortyApiModule
import com.example.rickandmortycharacters.ui.character_roster_fragment.CharacterRosterListAdapter
import com.retroroots.util.isNetworkAvailable
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.testing.UninstallModules
import dagger.hilt.components.SingletonComponent
import org.junit.Test
import retrofit2.Response
import java.net.UnknownHostException
import javax.inject.Singleton

@UninstallModules(RickAndMortyApiModule::class)
@HiltAndroidTest
class CharacterRosterFragmentTest
{
    companion object
    {
        val fakeCharacter = Character(
            "",
            emptyList(),
            "",
            1,
            "",
            Location("", ""),
            "test",
            Origin("", ""),
            "",
            "",
            "",
            ""
        )

        val fakeAllCharacters = AllCharacters(Page("", 0, ""), listOf(fakeCharacter, fakeCharacter.copy(id = 2)))

        fun navToDetailsFragment()
        {
            onView(withId(R.id.rosterRecVw)).perform(
                RecyclerViewActions.actionOnItemAtPosition<CharacterRosterListAdapter.ViewHolder>(0, click())
            )
        }
    }

    @Module
    @InstallIn(SingletonComponent::class)
    class FakeRickAndMortyApiModule
    {
        @Singleton
        @Provides
        fun providesRickAndMortyApi(@ApplicationContext context: Context) = object: RickAndMortyApi
        {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

            override suspend fun getCharacter(id: Int) =
                    when
                    {
                        connectivityManager.isNetworkAvailable() ->{
                            if(id < 0)
                                throw Exception()

                            Response.success(200, fakeCharacter)
                        }

                        else -> throw UnknownHostException()
                    }

            override suspend fun getAllCharacter(page: Int) =
                    when
                    {
                        connectivityManager.isNetworkAvailable() -> {
                            if(page < 0)
                                throw Exception()

                            Response.success(200, fakeAllCharacters)
                        }

                        else -> throw UnknownHostException()
                    }
        }
    }

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Before
    fun init()
    {
        hiltRule.inject()

        ActivityScenario.launch(MainActivity::class.java)
    }

    @Test
    fun recyclerviewChildCountTest()
    {
        onView(withId(R.id.rosterRecVw)).check(matches(hasChildCount(2)))
    }

    //Needs to be in airplane mode or purposely requesting the wrong data
    @Test
    fun warningViewIsVisibleOnErrorTest()
    {
        onView(withId(R.id.warningTxtVw)).check(matches(isDisplayed()))
    }

    @Test
    fun clickingRecyclerViewItemNavigatesToDetailsFragmentTest()
    {
        navToDetailsFragment()

        onView(withId(R.id.characterDataTxtVw)).check(matches(isDisplayed()))
    }
}