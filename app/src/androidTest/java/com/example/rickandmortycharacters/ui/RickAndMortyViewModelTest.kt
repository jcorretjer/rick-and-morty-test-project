package com.example.rickandmortycharacters.ui

import com.example.rickandmortycharacters.common.RickAndMortyDataTest
import com.example.rickandmortycharacters.data.common.data_source.all_characters.AllCharacters
import com.example.rickandmortycharacters.data.common.data_source.all_characters.Page
import com.example.rickandmortycharacters.data.common.data_source.character.Character
import com.example.rickandmortycharacters.data.common.data_source.character.Location
import com.example.rickandmortycharacters.data.common.data_source.character.Origin
import com.retroroots.network.state.ApiError
import com.retroroots.network.state.ResponseState
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
@HiltAndroidTest
class RickAndMortyViewModelTest : RickAndMortyDataTest
{
    private val fakeCharacter = Character(
        "",
        emptyList(),
        "",
        -1,
        "",
        Location("", ""),
        "",
        Origin("", ""),
        "",
        "",
        "",
        ""
    )

    private val fakeAllCharacters = AllCharacters(Page("", 0, ""), listOf(fakeCharacter, fakeCharacter.copy(id = 1)))

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    private val fakeRickAndMortyRepo = FakeRickAndMortyRepo()

    private lateinit var vm: RickAndMortyViewModel

    @Before
    fun init()
    {
        hiltRule.inject()
    }

    @Test
    override fun selectByInvalidIdTest()
    {
        runTest {

            vm = RickAndMortyViewModel(fakeRickAndMortyRepo, StandardTestDispatcher(testScheduler))

            assertTrue(vm.character.value is ResponseState.Loading)

            vm.getCharacter(-1)

            fakeRickAndMortyRepo.fakeCharacterServerResponse(ResponseState.Failure(ApiError.ServerError()))

            advanceUntilIdle()

            assertTrue((vm.character.value as ResponseState.Failure).error is ApiError.ServerError)
        }
    }

    @Test
    override fun selectByValidIdTest()
    {
        runTest {

            vm = RickAndMortyViewModel(fakeRickAndMortyRepo, StandardTestDispatcher(testScheduler))

            assertTrue(vm.character.value is ResponseState.Loading)

            vm.getCharacter(1)

            fakeRickAndMortyRepo.fakeCharacterServerResponse(ResponseState.Success(fakeCharacter))

            advanceUntilIdle()

            assertTrue((vm.character.value as ResponseState.Success).data.id == -1)
        }
    }

    @Test
    override fun selectByIdNoConnectionTest()
    {
        runTest {

            vm = RickAndMortyViewModel(fakeRickAndMortyRepo, StandardTestDispatcher(testScheduler))

            assertTrue(vm.character.value is ResponseState.Loading)

            vm.getCharacter(-1)

            fakeRickAndMortyRepo.fakeCharacterServerResponse(ResponseState.Failure(ApiError.NoInternetConnectionError(Exception())))

            advanceUntilIdle()

            assertTrue((vm.character.value as ResponseState.Failure).error is ApiError.NoInternetConnectionError)
        }
    }

    @Test
    override fun selectAllTest()
    {
        runTest {

            vm = RickAndMortyViewModel(fakeRickAndMortyRepo, StandardTestDispatcher(testScheduler))

            assertTrue(vm.allCharacters.value is ResponseState.Loading)

            vm.getAllCharacters()

            fakeRickAndMortyRepo.fakeAllCharacterServerResponse(ResponseState.Success(fakeAllCharacters))

            advanceUntilIdle()

            assertTrue((vm.allCharacters.value as ResponseState.Success).data.characters.first().id == -1)
        }
    }

    @Test
    override fun selectAllPage1Test()
    {
        runTest {

            vm = RickAndMortyViewModel(fakeRickAndMortyRepo, StandardTestDispatcher(testScheduler))

            assertTrue(vm.allCharacters.value is ResponseState.Loading)

            vm.getAllCharacters(1)

            fakeRickAndMortyRepo.fakeAllCharacterServerResponse(ResponseState.Success(fakeAllCharacters))

            advanceUntilIdle()

            assertTrue((vm.allCharacters.value as ResponseState.Success).data.characters.first().id == -1)
        }
    }

    @Test
    override fun selectAllNoConnectionTest()
    {
        runTest {

            vm = RickAndMortyViewModel(fakeRickAndMortyRepo, StandardTestDispatcher(testScheduler))

            assertTrue(vm.allCharacters.value is ResponseState.Loading)

            vm.getAllCharacters()

            fakeRickAndMortyRepo.fakeAllCharacterServerResponse(ResponseState.Failure(ApiError.NoInternetConnectionError(Exception())))

            advanceUntilIdle()

            assertTrue((vm.allCharacters.value as ResponseState.Failure).error is ApiError.NoInternetConnectionError)
        }
    }
}