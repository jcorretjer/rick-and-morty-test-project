package com.example.rickandmortycharacters.ui.character_roster_fragment.paged

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.example.rickandmortycharacters.data.repo.paging.RickAndMortyPagedRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PagedRosterViewModel @Inject constructor (pagedRepo: RickAndMortyPagedRepo): ViewModel()
{
    val pagedCharacters = pagedRepo.getAllCharacters()
        .cachedIn(viewModelScope)
}