package com.example.rickandmortycharacters.ui.character_roster_fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.rickandmortycharacters.databinding.FragmentCharacterRosterBinding
import com.example.rickandmortycharacters.ui.RickAndMortyViewModel
import com.retroroots.network.state.ResponseState
import com.retroroots.util.launchAndCollect
import com.retroroots.util.toggleVisibility
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers

@AndroidEntryPoint
class CharacterRosterFragment : Fragment()
{
    private var _binding : FragmentCharacterRosterBinding? = null

    private val binding get() = _binding!!

    private val rickAndMortyViewModel by viewModels<RickAndMortyViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View
    {
        _binding = FragmentCharacterRosterBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        val rosterListAdapter = CharacterRosterListAdapter{
            findNavController().navigate(CharacterRosterFragmentDirections.actionCharacterRosterFragmentToCharacterDetailsFragment(it))
        }

        binding.apply {
            rosterRecVw.let {
                it.setHasFixedSize(true)

                it.adapter = rosterListAdapter
            }

            loadingVwStb.let {
                it.inflate()

                it.toggleVisibility(false)
            }

            warningVwStb.let {
                it.inflate()

                it.toggleVisibility(false)
            }

            launchAndCollect(rickAndMortyViewModel.allCharacters, Dispatchers.Main) {

                loadingVwStb.toggleVisibility(it is ResponseState.Loading)

                warningVwStb.toggleVisibility(it is ResponseState.Failure)

                if(it is ResponseState.Success)
                    rosterListAdapter.submitList(it.data.characters)
            }

            rickAndMortyViewModel.getAllCharacters()
        }
    }

    override fun onDestroy()
    {
        super.onDestroy()

        _binding = null
    }
}