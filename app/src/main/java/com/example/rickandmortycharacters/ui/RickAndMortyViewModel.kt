package com.example.rickandmortycharacters.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.rickandmortycharacters.data.common.data_source.all_characters.AllCharacters
import com.example.rickandmortycharacters.data.common.data_source.character.Character
import com.example.rickandmortycharacters.data.repo.SharedRickAndMortyRepo
import com.retroroots.network.state.ApiError
import com.retroroots.network.state.ResponseState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RickAndMortyViewModel @Inject constructor(
    private val rickAndMortyRepo: SharedRickAndMortyRepo,
    private val dispatcher: CoroutineDispatcher
) : ViewModel()
{
    private val _allCharacters : MutableStateFlow<ResponseState<AllCharacters, ApiError>> = MutableStateFlow(ResponseState.Loading)

    val allCharacters : StateFlow<ResponseState<AllCharacters, ApiError>> = _allCharacters

    fun getAllCharacters(page : Int = 0)
    {
        viewModelScope.launch(dispatcher) {
            _allCharacters.value = rickAndMortyRepo.getAllCharacters(page)
        }
    }

    private val _character : MutableStateFlow<ResponseState<Character, ApiError>> = MutableStateFlow(ResponseState.Loading)

    val character : StateFlow<ResponseState<Character, ApiError>> = _character

    fun getCharacter(id : Int)
    {
        viewModelScope.launch(dispatcher) {
            _character.value = rickAndMortyRepo.getCharacter(id)
        }
    }
}