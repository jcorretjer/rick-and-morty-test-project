package com.example.rickandmortycharacters.ui.character_roster_fragment.paged

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.paging.LoadState
import com.example.rickandmortycharacters.databinding.FragmentCharacterRoster2Binding
import com.retroroots.util.launchAndCollect
import com.retroroots.util.toggleVisibility
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers

@AndroidEntryPoint
class PagedCharacterRosterFragment : Fragment()
{
    private var _binding : FragmentCharacterRoster2Binding? = null

    private val binding get() = _binding!!

    private val pagedRosterViewModel by viewModels<PagedRosterViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View
    {
        _binding = FragmentCharacterRoster2Binding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        binding.apply {

            loadingVwStb2.let {
                it.inflate()

                it.toggleVisibility(false)
            }

            warningVwStb2.let {
                it.inflate()

                it.toggleVisibility(false)
            }

            val rosterListAdapter = CharacterRosterPagedAdapter().apply {
                launchAndCollect(loadStateFlow, Dispatchers.Main) {
                    loadingVwStb2.toggleVisibility(it.refresh is LoadState.Loading)

                    warningVwStb2.toggleVisibility(it.refresh is LoadState.Error)
                }
            }

            rosterRecVw2.let {
                it.setHasFixedSize(true)

                it.adapter = rosterListAdapter
            }

            launchAndCollect(pagedRosterViewModel.pagedCharacters, Dispatchers.IO) {
                rosterListAdapter.submitData(it)
            }
        }
    }

    override fun onDestroy()
    {
        super.onDestroy()

        _binding = null
    }
}