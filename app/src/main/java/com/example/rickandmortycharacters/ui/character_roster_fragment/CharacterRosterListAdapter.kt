package com.example.rickandmortycharacters.ui.character_roster_fragment

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.example.rickandmortycharacters.R
import com.example.rickandmortycharacters.data.common.data_source.character.Character
import com.example.rickandmortycharacters.databinding.RosterItemBinding

class CharacterRosterListAdapter(private val onClickListener: (Int) -> Unit)
    : ListAdapter<Character, CharacterRosterListAdapter.ViewHolder>(DiffCallback())
{
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ViewHolder(RosterItemBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int)
    {
        holder.binding.apply {
            getItem(position).let { item ->
                holder.itemView.let { itemVw ->
                    Glide.with(itemVw.context)
                        .load(item.image)
                        .centerCrop()
                        .placeholder(R.drawable.baseline_account_circle_24)
                        .error(R.drawable.baseline_broken_image_24)
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .into(portraitImgVw)

                    itemVw.contentDescription = item.name

                    itemVw.setOnClickListener {
                        onClickListener(item.id)
                    }
                }
            }
        }
    }

    inner class ViewHolder(val binding: RosterItemBinding) : RecyclerView.ViewHolder(binding.root)

    class DiffCallback : DiffUtil.ItemCallback<Character>()
    {
        override fun areItemsTheSame(oldItem: Character, newItem: Character) = oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: Character, newItem: Character) = oldItem == newItem
    }
}