package com.example.rickandmortycharacters.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.example.rickandmortycharacters.databinding.FragmentCharacterDetailsBinding
import com.retroroots.network.state.ResponseState
import com.retroroots.util.launchAndCollect
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers

@AndroidEntryPoint
class CharacterDetailsFragment : Fragment()
{
    private var _binding : FragmentCharacterDetailsBinding? = null

    private val binding get() = _binding!!

    private val rickAndMortyViewModel by viewModels<RickAndMortyViewModel>()

    private val args : CharacterDetailsFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View
    {
        _binding = FragmentCharacterDetailsBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        launchAndCollect(rickAndMortyViewModel.character, Dispatchers.Main){
            binding.apply {

                findNavController().let {
                    characterToolbar.setupWithNavController(it, AppBarConfiguration(it.graph))
                }

                when(it)
                {
                    is ResponseState.Success ->
                        {
                            it.data.let { character ->
                                characterDataTxtVw.text = character.toString()

                                characterToolbar.title = character.name
                            }
                        }

                    is ResponseState.Failure -> characterDataTxtVw.text = it.error.toString()

                    else -> characterDataTxtVw.text = ResponseState.Loading.toString()
                }
            }
        }

        rickAndMortyViewModel.getCharacter(args.characterId)
    }

    override fun onDestroy()
    {
        super.onDestroy()

        _binding = null
    }
}