package com.example.rickandmortycharacters.data.repo.paging

import androidx.paging.Pager
import androidx.paging.PagingConfig
import com.example.rickandmortycharacters.data.remote.data_source.paging.RickAndMortyPagingSource

const val PAGE_SIZE = 20

const val MAX_SIZE = 100

class RickAndMortyPagedRepo (private val rickAndMortyPagingSource: RickAndMortyPagingSource) : SharedRickAndMortyPagedRepo
{
    override fun getAllCharacters() =
            Pager(
                PagingConfig(PAGE_SIZE,
                    enablePlaceholders = false,
                    maxSize = MAX_SIZE),
                pagingSourceFactory =  {
                    rickAndMortyPagingSource
                }
            ).flow
}