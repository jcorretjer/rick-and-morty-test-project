package com.example.rickandmortycharacters.data.common.data_source.character

data class Origin(
    val name: String,
    val url: String
)