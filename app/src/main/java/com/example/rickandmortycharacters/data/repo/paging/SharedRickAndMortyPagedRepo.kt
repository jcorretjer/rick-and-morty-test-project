package com.example.rickandmortycharacters.data.repo.paging

import androidx.paging.PagingData
import com.example.rickandmortycharacters.data.common.data_source.character.Character
import kotlinx.coroutines.flow.Flow

/**
 * This is only used for writing view model test
*/

interface SharedRickAndMortyPagedRepo
{
    fun getAllCharacters() : Flow<PagingData<Character>>
}