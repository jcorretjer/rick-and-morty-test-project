package com.example.rickandmortycharacters.data.repo

import com.example.rickandmortycharacters.data.remote.data_source.RickAndMortyRemoteDataSource

class RickAndMortyRepo (private val rickAndMortyRemoteDataSource: RickAndMortyRemoteDataSource) : SharedRickAndMortyRepo
{
    override suspend fun getAllCharacters(page: Int) = rickAndMortyRemoteDataSource.selectAll(page)

    override suspend fun getCharacter(id: Int) = rickAndMortyRemoteDataSource.selectById(id)
}