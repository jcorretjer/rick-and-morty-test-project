package com.example.rickandmortycharacters.data.remote.data_source

import com.example.rickandmortycharacters.data.common.data_source.character.Character
import com.example.rickandmortycharacters.data.remote.api.RickAndMortyApi
import com.retroroots.network.state.ApiError
import com.retroroots.network.data_source.RemoteDataSource
import com.retroroots.network.state.ResponseState

class RickAndMortyRemoteDataSource(private val rickAndMortyApi: RickAndMortyApi) : RemoteDataSource<Character>()
{
    override suspend fun selectById(id: Int): ResponseState<Character, ApiError> =
            try
            {
                processResponse(rickAndMortyApi.getCharacter(id))
            }
            catch (e: Exception)
            {
                processException(e)
            }

    suspend fun selectAll(page: Int) =
            try
            {
                processResponse(rickAndMortyApi.getAllCharacter(page))
            }
            catch (e: Exception)
            {
                processException(e)
            }
}