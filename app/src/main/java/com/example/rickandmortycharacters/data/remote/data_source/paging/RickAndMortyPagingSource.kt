package com.example.rickandmortycharacters.data.remote.data_source.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.rickandmortycharacters.data.common.data_source.character.Character
import com.example.rickandmortycharacters.data.remote.api.RickAndMortyApi
import com.retroroots.network.state.ApiError
import retrofit2.HttpException
import java.io.IOException

private const val INITIAL_PAGE_POSITION = 0

class RickAndMortyPagingSource(private val rickAndMortyApi: RickAndMortyApi) : PagingSource<Int, Character>()
{
    override fun getRefreshKey(state: PagingState<Int, Character>): Int?
    {
        // Try to find the page key of the closest page to anchorPosition from
        // either the prevKey or the nextKey; you need to handle nullability
        // here.
        //  * prevKey == null -> anchorPage is the first page.
        //  * nextKey == null -> anchorPage is the last page.
        //  * both prevKey and nextKey are null -> anchorPage is the
        //    initial page, so return null.
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition((anchorPosition)).let {
                it?.prevKey?.plus(1) ?: it?.nextKey?.minus(1)
            }
        }
    }


    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Character>
    {
        try
        {
            (params.key ?: INITIAL_PAGE_POSITION).let {
                rickAndMortyApi.getAllCharacter(it).let { response ->
                    response.body()?.let { body ->
                        if (response.isSuccessful)
                            return LoadResult.Page(body.characters, body.page.prev, body.page.next)
                    }
                }
            }

            throw ApiError.SERVER_ERROR_EXCEPTION
        }
        catch (e: IOException)
        {
            return LoadResult.Error(e)
        }
        catch (e: HttpException)
        {
            return LoadResult.Error(e)
        }
    }
}