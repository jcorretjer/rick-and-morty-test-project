package com.example.rickandmortycharacters.data.repo

import com.example.rickandmortycharacters.data.common.data_source.all_characters.AllCharacters
import com.example.rickandmortycharacters.data.common.data_source.character.Character
import com.retroroots.network.state.ApiError
import com.retroroots.network.state.ResponseState

/**
 * This is only used for writing view model test
*/

interface SharedRickAndMortyRepo
{
    suspend fun getAllCharacters(page : Int) : ResponseState<AllCharacters, ApiError>

    suspend fun getCharacter(id : Int) : ResponseState<Character, ApiError>
}