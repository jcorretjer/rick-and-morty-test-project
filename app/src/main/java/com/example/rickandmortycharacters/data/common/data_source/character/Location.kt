package com.example.rickandmortycharacters.data.common.data_source.character

data class Location(
    val name: String,
    val url: String
)