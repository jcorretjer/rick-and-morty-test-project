package com.example.rickandmortycharacters.data.common.data_source.all_characters

import com.example.rickandmortycharacters.data.common.data_source.character.Character
import com.google.gson.annotations.SerializedName

data class AllCharacters(
    @SerializedName("info")
    val page: Page,
    @SerializedName("results")
    val characters: List<Character>
)