package com.example.rickandmortycharacters.data.remote.api

import com.example.rickandmortycharacters.BuildConfig
import com.example.rickandmortycharacters.data.common.data_source.all_characters.AllCharacters
import com.example.rickandmortycharacters.data.common.data_source.character.Character
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

const val ID_PATH = "id"

interface RickAndMortyApi
{
    companion object
    {
        const val BASE_URL = BuildConfig.RICK_AND_MORTYAPI_BASE_URL
    }

    @GET("character/{$ID_PATH}")
    suspend fun getCharacter(@Path(ID_PATH) id : Int) : Response<Character>

    @GET("character/")
    suspend fun getAllCharacter(@Query("page") page : Int) : Response<AllCharacters>
}