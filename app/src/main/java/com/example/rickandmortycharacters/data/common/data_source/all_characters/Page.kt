package com.example.rickandmortycharacters.data.common.data_source.all_characters

import com.google.gson.annotations.SerializedName

data class Page(
    @SerializedName("next")
    private val _next: String?,
    val pages: Int,
    @SerializedName("prev")
    private val _prev: String?
)
{
    val next : Int?
        get() = getPageNumberOrNull(_next)

    val prev : Int?
        get() = getPageNumberOrNull(_prev)

    companion object
    {
        fun getPageNumberOrNull(pageUrl : String?) =
                try
                {
                    pageUrl?.split("page=")?.last()?.toInt()
                }

                catch (_: Exception)
                {
                    null
                }
    }
}