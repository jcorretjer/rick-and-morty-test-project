package com.example.rickandmortycharacters.data.remote.di

import com.example.rickandmortycharacters.data.repo.RickAndMortyRepo
import com.example.rickandmortycharacters.data.remote.api.RickAndMortyApi
import com.example.rickandmortycharacters.data.remote.data_source.RickAndMortyRemoteDataSource
import com.example.rickandmortycharacters.data.remote.data_source.paging.RickAndMortyPagingSource
import com.example.rickandmortycharacters.data.repo.SharedRickAndMortyRepo
import com.example.rickandmortycharacters.data.repo.paging.RickAndMortyPagedRepo
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object RickAndMortyNetworkModule
{
    @Singleton
    @Provides
    fun providesRickAndMortyDataSource(rickAndMortyApi: RickAndMortyApi) = RickAndMortyRemoteDataSource(rickAndMortyApi)

    @Singleton
    @Provides fun providesRickAndMortyRepo(rickAndMortyRemoteDataSource: RickAndMortyRemoteDataSource) : SharedRickAndMortyRepo
            = RickAndMortyRepo(rickAndMortyRemoteDataSource)

    @Singleton
    @Provides
    fun providesRickAndMortyRetrofit(): Retrofit = Retrofit.Builder()
        .baseUrl(RickAndMortyApi.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    @Singleton
    @Provides
    fun providesRickAndMortyPagingSource(rickAndMortyApi: RickAndMortyApi) = RickAndMortyPagingSource(rickAndMortyApi)

    @Singleton
    @Provides fun providesRickAndMortyPagedRepo(rickAndMortyPagingSource: RickAndMortyPagingSource) = RickAndMortyPagedRepo(rickAndMortyPagingSource)
}