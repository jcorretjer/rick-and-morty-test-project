package com.retroroots.util

import android.net.ConnectivityManager
import android.net.NetworkCapabilities

fun ConnectivityManager.isNetworkAvailable() : Boolean
{
    getNetworkCapabilities(activeNetwork)?.let {
       return  it.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
    }

    return false
}